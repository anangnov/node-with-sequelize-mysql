const userController = require('../controllers').userController;
const employeeController = require('../controllers').employeeController;

module.exports = (app) => {
	app.get('/api', (req, res) => res.status(200).send({
		message: 'API testing sample'
	}));

	app.get('/api/user', userController.showAll);
	app.post('/api/user/create', userController.create);
	app.get('/api/user/:id', userController.showById);
	app.post('/api/user/update/:id', userController.update);
	app.post('/api/user/delete/:id', userController.delete);

	app.get('/api/employee', employeeController.showAll);
	app.post('/api/employee/create', employeeController.create);
	app.get('/api/employee/:id', employeeController.showById);
	app.post('/api/employee/update/:id', employeeController.update);
	app.post('/api/employee/delete/:id', employeeController.delete);
};