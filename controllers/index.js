const userController = require('./userController');
const employeeController = require('./employeeController');

module.exports = {
	userController,
	employeeController
};