'use-strict';

const User = require('../models').User;

module.exports = {
	create(req, res) {
		return User.create({
			name: req.body.name,
			address: req.body.address,
			email: req.body.email
		})
		.then(user => res.status(201).json({
			status: true,
			message: 'success',
			data: user
		}))
		.catch(error => res.status(400).send(error));
	},

	showAll(req, res) {
		return User.findAll()
		.then(user => res.status(200).json({
			status: true,
			message: 'success',
			data: user
		}))
		.catch(error => res.status(400).send(error))
	},

	showById(req, res) {
		return User.findOne({ where: {id: req.params.id} })
		.then(user => 
			user ? res.status(200).json({
				status: true,
				message: 'success',
				data: user
			}) 
			: res.json({
				status: false,
				message: 'null'
			})
		)
		.catch(error => res.status(400).send(error))
	},

	update(req, res) {
		return User.update(
			{
				name: req.body.name,
				address: req.body.address,
				email: req.body.email
			},
			{ 
				where: {id: req.params.id} 
			}
		)
		.then(user => res.status(200).json({
			status: true,
			message: 'success',
			data: {
				name: req.body.name,
				address: req.body.address,
				email: req.body.email
			}
		}))
		.catch(error => res.status(400).send(error))
	},

	delete(req, res) {
		return User.destroy(
			{ 
				where: {id: req.params.id} 
			}
		)
		.then(user => res.status(200).json({
			status: true,
			message: 'success'
		}))
		.catch(error => res.status(400).send(error))
	}
};