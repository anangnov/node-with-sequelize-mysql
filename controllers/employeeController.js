'use-strict';

const Employee = require('../models').Employee;

module.exports = {
	create(req, res) {
		return Employee.create({
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email
		})
		.then(employee => res.status(201).json({
			status: true,
			message: 'success',
			data: employee
		}))
		.catch(error => res.status(400).send(error))
	},

	showAll(req, res) {
		return Employee.findAll()
		.then(employee => res.status(200).json({
			status: true,
			message: 'success',
			data: employee
		}))
		.catch(error => res.status(400).send(error))
	},

	showById(req, res) {
		return Employee.findOne({ where: {id: req.params.id} })
		.then(employee => 
			employee ? 
			res.status(200).json({
				status: true,
				message: 'success',
				data: employee
			})
			: res.json({
				status: false,
				message: 'null'
			})
		)
		.catch(error => res.status(400).send(error))
	},

	update(req, res) {
		return Employee.update(
			{
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email
			},
			{ 
				where: {id: req.params.id} 
			}
		)
		.then(employee => res.status(200).json({
			status: true,
			message: 'success',
			data: {
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email
			}
		}))
		.catch(error => res.status(400).send(error))
	}, 

	delete(req, res) {
		return Employee.destroy(
			{ 
				where: {id: req.params.id} 
			}
		)
		.then(employee => res.status(200).json({
			status: true,
			message: 'success'
		}))
		.catch(error => res.status(400).send(error))
	}
};
