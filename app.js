const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const http = require('http');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Models
const models = require('./models');
models.sequelize.sync().then(function() {
	console.log('Database has connected');
}).catch(function(err) {
	console.log('Something error');
})

// Routes
require('./routes')(app);
app.get('*', (req, res) => res.status(200).send({
	message: 'How are you today?'
}));

const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);

const server = http.createServer(app);
server.listen(port);

module.exports = app;
